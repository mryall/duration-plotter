#!/usr/local/bin/python

from matplotlib import pyplot, dates
from os import path
import numpy
import sys
import dateutil
import pandas as pd
import code

time, duration = numpy.loadtxt(sys.argv[1], unpack=True,
    converters={ 0: lambda d: dates.date2num(dateutil.parser.parse(d)) })
data = pd.DataFrame(dict(date=[dates.num2date(t) for t in time], duration=duration))
date_index = data.set_index('date')
analysed = date_index.groupby(lambda d: d.replace(second=0)).agg([ numpy.mean, lambda x: x.quantile(0.9) ])

# open interactive python
# code.interact(local=locals())

pyplot.plot_date(x=time, y=duration, fmt='.', color='0.75', markersize=3, label='time')
pyplot.plot_date(x=analysed.index, y=analysed.duration['mean'], fmt='b-', label='mean')
pyplot.plot_date(x=analysed.index, y=analysed.duration['<lambda>'], fmt='r-', label='90%')
pyplot.yscale('log')
pyplot.ylabel('request time (s)')
pyplot.xlabel('date')
pyplot.legend()

out_file = path.basename(path.splitext(sys.argv[1])[0]) + '.png'
pyplot.savefig(out_file)
print "Saved plot to %s" % out_file

